name: 'gitlab_snowflake'
version: '1.0'

profile: 'gitlab-snowflake'

source-paths: ["models"]
test-paths: ["tests"]
data-paths: ["data"]
macro-paths: ["macros"]

target-path: "target"
clean-targets:
    - "target"
    - "dbt_modules"

quoting:
    database: true
    identifier: false
    schema: false

archive:
    - source_schema: analytics_staging
      target_schema: dbt_archive
      tables:
        - source_table: sfdc_account
          target_table: sfdc_account_archived
          updated_at: _last_dbt_run::timestamp_ntz
          unique_key: account_id

        - source_table: sfdc_opportunity
          target_table: sfdc_opportunity_archived
          updated_at: _last_dbt_run::timestamp_ntz
          unique_key: opportunity_id

        - source_table: sfdc_users
          target_table: sfdc_users_archived
          updated_at: _last_dbt_run::timestamp_ntz
          unique_key: user_id_18__c

on-run-start:
    - "{{resume_warehouse(var('resume_warehouse', false), var('warehouse_name'))}}"
    - "{{create_udfs()}}"

on-run-end:
    - "{{grant_usage_to_schemas(schema_name, rolename)}}"
    - "{{suspend_warehouse(var('suspend_warehouse', false), var('warehouse_name'))}}"

models:
    pre-hook: "{{ logging.log_model_start_event() }}"
    post-hook: "{{ logging.log_model_end_event() }}"
    vars:
      database: 'raw'
      warehouse_name: "{{ env_var('SNOWFLAKE_TRANSFORM_WAREHOUSE') }}"

    logging:
      schema: analytics_meta
      post-hook:
        - "grant select on {{this}} to role reporter"

    snowplow:
      schema: analytics
      tags: ["product"]
      post-hook: "grant select on {{this}} to role reporter"
      vars:
        'snowplow:use_fivetran_interface': false
        'snowplow:events': "{{ref('snowplow_unnested_events')}}"
        'snowplow:context:web_page': "{{ref('snowplow_web_page')}}"
        'snowplow:context:performance_timing': false
        'snowplow:context:useragent': false
        'snowplow:timezone': 'America/New_York'
        'snowplow:page_ping_frequency': 30
        'snowplow:app_ids': ['gitlab']
        'snowplow:pass_through_columns': ['cf_formid','cf_elementid','cf_nodename','cf_type','cf_elementclasses','cf_value','sf_formid','sf_formclasses','sf_elements','ff_formid','ff_elementid','ff_nodename','ff_elementtype','ff_elementclasses','ff_value','lc_elementid','lc_elementclasses','lc_elementtarget','lc_targeturl','lc_elementcontent','tt_category','tt_variable','tt_timing','tt_label']
      base:
        materialized: view
        optional:
          enabled: false
      page_views:
        optional:
          enabled: false

    gitlab_snowflake:
      enabled: true
      materialized: view

      bamboohr:
        base:
          enabled: true
          materialized: table

      customers:
        base:
          enabled: true
          materialized: table

      date:
        base:
          enabled: true
          materialized: table

      dbt_logging:
        enabled: false

      gitter:
        enabled: false
        transformed:
          materialized: table

      gitlab_dotcom:
        tags: ["product"]
        enabled: true
        xf:
          materialized: table


      netsuite_fivetran:
        enabled: true
        base:
          materialized: table
        xf:
          materialized: table

      netsuite_stitch:
        enabled: true
        base:
          materialized: table
        xf:
          materialized: table

      pings:
        tags: ["product"]
        xf:
          enabled: true
          materialized: table

      pings_to_sfdc:
        tags: ["product"]
        enabled: true
        materialized: table

      pipe2spend:
        enabled: false
        materialized: table

      retention:
        enabled: true
        materialized: table

      snowplow:
        tags: ["product"]
        enabled: true
        xf:
          materialized: table

      snowflake:
        base:
          enabled: true
          materialized: table
        xf:
          enabled: true
          materialized: table

      sfdc:
        xf:
          enabled: true
          materialized: table

      zuora:
        xf:
          enabled: true
          materialized: table

      zendesk:
        xf:
          enabled: true
          materialized: table
